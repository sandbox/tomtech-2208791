<?php

/**
 * @file
 * Provides automatic generation of favicon an touch icons
 * and automatic creation of the appropriate HEAD LINK tags.
 */

// Define the library name.
define('ICONIFIED_PHP_ICO_LIBRARY', 'php-ico');

/**
 * Implements hook_menu().
 */
function iconified_menu() {
  $menu = array();
  $menu['admin/content/iconified'] = array(
    'title' => 'Iconified',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('iconified_settings_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer iconified'),
    'type' => MENU_NORMAL_ITEM,
    'description' => 'Administer site iconification',
    'file' => 'iconified.admin.inc',
  );
  return $menu;
}

/**
 * Implements hook_permission().
 */
function iconified_permission() {
  $permissions = array();
  $permissions['administer iconified'] = array(
    'title' => 'administer iconified',
    'description' => t('Administer iconified'),
    'restrict access' => FALSE,
  );
  return $permissions;
}

/**
 * Implements hook_theme().
 */
function iconified_theme() {
  return array(
    'iconified_image' => array(
      'variables' => array(
        'image' => NULL,
        'style' => NULL,
      ),
      'file' => 'iconified.theme.inc',
    ),
  );
}

/**
 * Implements hook_libraries_info().
 */
function iconified_libraries_info() {
  $libraries[ICONIFIED_PHP_ICO_LIBRARY] = array(
    'name' => ICONIFIED_PHP_ICO_LIBRARY,
    'vendor url' => 'https://github.com/chrisbliss18/php-ico',
    'download url' => 'https://github.com/chrisbliss18/php-ico/archive/master.zip',
    'version arguments' => array(
      'file' => 'changelog',
      'pattern' => '@([0-9a-zA-Z\.-]+) @',
      'lines' => 10,
    ),
    'files' => array(
      'php' => array(
        'class-php-ico.php',
      ),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_preprocess_html().
 */
function iconified_preprocess_html(&$vars) {
  $path = file_default_scheme() . '://iconified/';
  $icons = iconified_icons();
  foreach ($icons['favicon'] as $key => $favicon) {
    iconified_add_html_head_link($favicon['rel'], $path . $key, $favicon['type']);
  }
  foreach ($icons['touch-icons'] as $key => $touch_icon) {
    iconified_add_html_head_link($touch_icon['rel'], $path . $key, NULL, $touch_icon['sizes']);
  }
}

/**
 * Returns the list of supported touch icons and attributes.
 * @return array
 *   The array of icon files.
 */
function iconified_icons() {
  $files = array(
    'touch-icons' => array(
      'apple-touch-icon.png' => array(
        'rel' => 'apple-touch-icon',
        'height' => '57',
        'width' => '57',
      ),
      'apple-touch-icon-57x57.png' => array(
        'rel' => 'apple-touch-icon',
        'height' => '57',
        'width' => '57',
        'sizes' => '57x57',
      ),
      'apple-touch-icon-60x60.png' => array(
        'rel' => 'apple-touch-icon',
        'height' => '60',
        'width' => '60',
        'sizes' => '60x60',
      ),
      'apple-touch-icon-72x72.png' => array(
        'rel' => 'apple-touch-icon',
        'height' => '72',
        'width' => '72',
        'sizes' => '72x72',
      ),
      'apple-touch-icon-76x76.png' => array(
        'rel' => 'apple-touch-icon',
        'height' => '76',
        'width' => '76',
        'sizes' => '76x76',
      ),
      'apple-touch-icon-114x114.png' => array(
        'rel' => 'apple-touch-icon',
        'height' => '114',
        'width' => '114',
        'sizes' => '114x114',
      ),
      'apple-touch-icon-120x120.png' => array(
        'rel' => 'apple-touch-icon',
        'height' => '120',
        'width' => '120',
        'sizes' => '120x120',
      ),
      'apple-touch-icon-144x144.png' => array(
        'rel' => 'apple-touch-icon',
        'height' => '144',
        'width' => '144',
        'sizes' => '144x144',
      ),
      'apple-touch-icon-152x152.png' => array(
        'rel' => 'apple-touch-icon',
        'height' => '152',
        'width' => '152',
        'sizes' => '152x152',
      ),
    ),
    'favicon' => array(
      'favicon.ico' => array(
        'rel' => 'shortcut icon',
        'type' => 'image/vnd.microsoft.icon',
      ),
    ),
  );

  return $files;
}

/**
 * Adds an HTML HEAD LINK based on the parameters supplied.
 * 
 * @param string $rel
 *   The $rel attribute of the link element.
 * @param string $uri
 *   The $uri attribute of the link element.
 * @param string $type
 *   The $type attribute of the link element.
 * @param array $sizes
 *   The sizes attribute for the link element.
 */
function iconified_add_html_head_link($rel, $uri, $type = NULL, $sizes = NULL) {
  $attributes = array(
    'rel' => $rel,
    'href' => file_create_url($uri),
  );
  if (!empty($type)) {
    $attributes['type'] = $type;
  }
  if (!empty($sizes)) {
    $attributes['sizes'] = $sizes;
  }
  drupal_add_html_head_link($attributes);
}

/**
 * Implements hook_html_head_alter().
 * 
 * Alters the HEAD element to remove any other shortcut icon LINKs,
 * as there should be only one.
 */
function iconified_html_head_alter(&$head_elements) {
  foreach ($head_elements as $key => $element) {
    if (stripos($key, 'shortcut icon') !== FALSE && stripos($key, 'iconified') === FALSE) {
      unset($head_elements[$key]);
    }
  }
}
