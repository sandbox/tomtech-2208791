<?php

/**
 * @file
 * Code to provide administration of the iconified module.
 */

/**
 * Form constructor.
 *
 * Settings form for specifying an icon
 * and reviewing the resulting generated images.
 *
 * @param array $form
 *   A structured FAPI $form array.
 * @param array $form_state
 *   The $form_state array for the form this element belongs to.
 * 
 * @return array
 *   The form.
 */
function iconified_settings_form($form, &$form_state) {
  if ($image_fid = variable_get('iconified_image_fid', FALSE)) {
    $image = file_load($image_fid);
    $style = 'original';
    $form['image'] = array(
      '#markup' => theme('iconified_image', array('image' => $image, 'style' => $style)),
    );
  }
  $form['iconified_image_fid'] = array(
    '#title' => t('Iconified source image'),
    '#type' => 'managed_file',
    '#upload_location' => file_default_scheme() . '://iconified/source/',
    '#default_value' => $image_fid,
    '#progress_indicator' => 'bar',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      'file_validate_size' => array(524288 * 4),
    ),
    '#theme' => 'file_managed_file',
    '#weight' => -10,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => -5,
  );
  return $form;
}

/**
 * Form validation handler for iconified_settings_form().
 *
 * @param array $form
 *   A structured FAPI $form array.
 * @param array $form_state
 *   The $form_state array for the form this element belongs to.
 */
function iconified_settings_form_validate($form, &$form_state) {
  if (!isset($form_state['values']['iconified_image_fid']) || !is_numeric($form_state['values']['iconified_image_fid'])) {

  }
  else {
    $fid = $form_state['values']['iconified_image_fid'];
    $file = file_load($fid);
    if ($file->info['width'] != $file->info['height']) {
      form_set_error('iconified_image_fid', t('Please select an image that is square.'));
      file_delete($file);
    }
  }
}

/**
 * Form submission handler for iconified_settings_form().
 *
 * @param array $form
 *   A structured FAPI $form array.
 * @param array $form_state
 *   The $form_state array for the form this element belongs to.
 */
function iconified_settings_form_submit($form, &$form_state) {
  if ($form_state['values']['iconified_image_fid'] != 0) {
    $file = file_load($form_state['values']['iconified_image_fid']);
    if ($file->status != FILE_STATUS_PERMANENT) {
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      file_usage_add($file, 'iconified', 'file', 1);
      variable_set('iconified_image_fid', $file->fid);
    }
    iconified_generate_images();
  }
  elseif ($form_state['values']['iconified_image_fid'] == 0) {
    $fid = variable_get('iconified_image_fid', FALSE);
    $file = $fid ? file_load($fid) : FALSE;
    if ($file) {
      file_usage_delete($file, 'iconified', 'file', 1);
      file_delete($file);
    }
    $path = file_default_scheme() . '://iconified/';
    $icons = iconified_icons();
    foreach ($icons['favicon'] as $key => $favicon) {
      file_unmanaged_delete($path . $key);
    }
    foreach ($icons['touch-icons'] as $key => $touch_icon) {
      file_unmanaged_delete($path . $key);
    }
    variable_set('iconified_image_fid', FALSE);
  }
}

/**
 * Verify the uploaded image exists and create the favicon and touch icons.
 */
function iconified_generate_images() {
  $fid = variable_get('iconified_image_fid', FALSE);
  $file = $fid ? file_load($fid) : FALSE;
  $path = file_default_scheme() . '://iconified/';
  if ($file) {
    $icons = iconified_icons();
    foreach ($icons['favicon'] as $key => $favicon) {
      iconified_generate_favico($file->uri, $path . $key);
    }
    foreach ($icons['touch-icons'] as $key => $touch_icon) {
      iconified_generate_image($file->uri, $touch_icon['width'], $touch_icon['height'], $path . $key);
    }
  }
}

/**
 * Scales the image at $uri to dimensions $width,$height and saves it to $path.
 * 
 * @param string $uri
 *   The $uri of the source image file.
 * @param int $width
 *   The $width of the destination image.
 * @param int $height
 *   The $height of the destination image.
 * @param string $path
 *   The $path of the destination image.
 */
function iconified_generate_image($uri, $width, $height, $path) {
  if ($image = image_load($uri)) {
    if (image_scale($image, $width, $height)) {
      image_save($image, $path);
    }
    imagedestroy($image);
  }
}

/**
 * Using the image at $uri, generates a favicon.
 * 
 * @param string $uri
 *   The $uri of the source image.
 * @param string $destination_uri
 *   The $destination_uri of the favicon.
 */
function iconified_generate_favico($uri, $destination_uri) {
  if (module_exists('libraries') && file_exists(libraries_get_path(PHP_ICO_LIBRARY) . "/class-php-ico.php")) {
    require libraries_get_path(PHP_ICO_LIBRARY) . "/class-php-ico.php";

    $source = drupal_realpath($uri);
    $destination = drupal_realpath($destination_uri);

    $sizes = array(
      array(16, 16),
      array(24, 24),
      array(32, 32),
      array(48, 48),
      array(64, 64),
    );

    $ico_lib = new PHP_ICO($source, $sizes);
    $ico_lib->save_ico($destination);
  }
  else {
    drupal_set_message('php-ico library does not appear to be installed. The favicon was NOT generated.', 'warning');
  }
}
