<?php
/**
 * @file
 * Provides theming for iconified images.
 */

/**
 * Themes an iconified image, returning generated favicon and touch icon images.
 * 
 * @param array $variables
 *   The variables for the themed element.
 *
 * @return string
 *   The generated html for the element.
 */
function theme_iconified_image($variables) {
  drupal_add_css(drupal_get_path('module', 'iconified') . '/css/iconified.css');
  $image = $variables['image'];
  $output = '<div class="iconified">';

  $path = file_default_scheme() . '://iconified/';
  $icons = iconified_icons();
  foreach ($icons['favicon'] as $key => $favicon) {
    $output .= iconified_image($path . $key);
  }
  foreach ($icons['touch-icons'] as $key => $touch_icon) {
    $output .= iconified_image($path . $key);
  }
  $output .= iconified_image($image->uri, 'Original');
  $output .= '</div>';
  return $output;
}

/**
 * Returns a specific iconified image.
 *
 * @param string $uri
 *   The $uri of the image.
 * @param string $caption
 *   The $caption of the image.
 * 
 * @return string
 *   The generated image HTML.
 */
function iconified_image($uri, $caption = NULL) {
  if (empty($caption)) {
    $caption = basename($uri);
  }
  return '<figure><figcaption>' . $caption . '</figcaption>' . theme('image', array(
        'path' => $uri,
        'getsize' => FALSE,
      )) . '</figure>';
}
