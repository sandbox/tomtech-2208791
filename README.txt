Description
-----------
This module allows you to upload an image and it will create 
a favicon and touch icons.

The favicon uses Chris Bliss's php-ico library to generate the favicon.

<https://github.com/chrisbliss18/php-ico>.

You will need to download it for the favicon to be created.

If you enable the module with drush, the library should automatically install.

There is also a drush command to download and install the library.
