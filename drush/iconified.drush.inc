<?php

/**
 * @file
 * Code to add drush commands for iconified.
 */

// Define the download uri.
define('ICONIFIED_PHP_ICO_DOWNLOAD_URI', 'https://github.com/chrisbliss18/php-ico/archive/master.zip');

/**
 * Implements hook_drush_command().
 */
function iconified_drush_command() {
  $items['php-ico-download'] = array(
    'callback' => 'iconified_drush_download',
    'description' => dt('Downloads the required PHP ICO library from https://github.com/chrisbliss18/php-ico.'),
    'arguments' => array(
      'path' => dt('Optional. The path to the download folder. If omitted, Drush will use the default location (<code>sites/all/libraries/php-ico</code>).'),
    ),
  );
  return $items;
}

/**
 * Callback for drush download command.
 */
function iconified_drush_download() {
  $args = func_get_args();
  if ($args[0]) {
    $path = $args[0];
  }
  else {
    $path = drush_get_context('DRUSH_DRUPAL_ROOT') . '/sites/all/libraries/php-ico';
  }
  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Download the zip archive.
  if ($filepath = drush_download_file(ICONIFIED_PHP_ICO_DOWNLOAD_URI)) {
    $filename = basename($filepath);

    // Remove any existing php-ico plugin directory.
    if (is_dir('php-ico-master')) {
      drush_delete_dir('php-ico-master', TRUE);
      drush_log(dt('An existing php-ico plugin was deleted from @path', array('@path' => $path)), 'notice');
    }

    // Decompress the zip archive.
    drush_tarball_extract($filename, '../');
    drush_move_dir('../php-ico-master', '../php-ico', TRUE);
  }

  if (is_dir('../php-ico')) {
    drush_log(dt('php-ico plugin has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the php-ico plugin to @path', array('@path' => $path)), 'error');
  }
  // Set working directory back to the previous working directory.
  chdir($olddir);
}

/**
 * Implements drush_MODULE_post_COMMAND().
 */
function drush_iconified_post_enable() {
  $modules = func_get_args();
  if (in_array('iconified', $modules)) {
    iconified_drush_download();
  }
}
